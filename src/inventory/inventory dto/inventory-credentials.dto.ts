//Importando dependencias
import { IsString, IsNotEmpty, IsNumberString } from 'class-validator';

//Esta clase se encarga de definir la estructura de los datos del inventario.
//Siendo así la encargada de definir los tipos de datos, decoradores y demás que
//serán la base de esta área de la aplicación
export class InventoryCredentialsDto {
  @IsNotEmpty()
  sku: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsNumberString()
  amount: number;

  @IsNotEmpty()
  @IsNumberString()
  prize: number;

  @IsNotEmpty()
  @IsString()
  description: string;

  image: string;
}
