//Importando dependencias
import { Module } from '@nestjs/common';
import { InventoryController } from './inventory.controller';
import { InventoryService } from './inventory.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductRepository } from './inventory.repository';
import { AuthModule } from 'src/Auth/auth.module';
import { UsersRepository } from 'src/Auth/users.repository';
import { AuthCredentialsDto } from 'src/Auth/Auth dto/auth-credentials.dto';

//Este apartado contiene la configuración necesaria para
//enlazar todo el programa de la sección del inventario
@Module({
  imports: [
    TypeOrmModule.forFeature([ProductRepository, UsersRepository]),
    AuthModule,
    AuthCredentialsDto,
  ],
  controllers: [InventoryController],
  providers: [InventoryService],
})
export class InventoryModule {}
