//Importando dependencias
import { User } from 'src/Auth/users.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

//En esta entidad se define la estructura que tendrá la base de datos
@Entity()
export class Product {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  sku: string;

  @Column()
  name: string;

  @Column()
  amount: number;

  @Column()
  prize: number;

  @Column()
  description: string;

  @Column()
  image: string;

  @ManyToOne((_type) => User, (user) => user.products, {
    onDelete: 'CASCADE',
  })
  user: User;
}
