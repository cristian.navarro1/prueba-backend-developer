//Importando dependencias
import {
  Body,
  Controller,
  Delete,
  Patch,
  Post,
  Res,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { GetUser } from 'src/Auth/get-user.decorator';
import { User } from 'src/Auth/users.entity';
import { InventoryCredentialsDto } from './inventory dto/inventory-credentials.dto';
import { Product } from './inventory.entity';
import { InventoryService } from './inventory.service';
import { v4 as uuidv4 } from 'uuid';
import path = require('path');
import { diskStorage } from 'multer';
import { PaginationOptionsInterface } from './paginate/paginatio.options.interface';
import { Pagination } from './paginate/pagination';

//EL siguiente fragmento de archivo se encarga de definir la ruta
//para guardar las imágenes subidas, así como de generar el nombre
//de este archivo
export const storage = {
  storage: diskStorage({
    destination: './productImages',
    filename: (req, uploadImage, cb) => {
      if (uploadImage) {
        const filename: string =
          path.parse(uploadImage.originalname).name.replace(/\s/g, '') +
          uuidv4();
        const extension: string = path.parse(uploadImage.originalname).ext;

        cb(null, `${filename}${extension}`);
      }
    },
  }),
};

//En esta sección es muy importante el token generado al iniciar
//sesión, ya que sin él no será posible acceder a estos endpoints
@UseGuards(AuthGuard())
@Controller('inventory')
export class InventoryController {
  constructor(private inventoryService: InventoryService) {}

  //Este controlador se utiliza para agregar un nuevo producto a
  //la base de datos y lo asocia con el usuario que lo creó
  @Post('/addproduct')
  @UseInterceptors(FileInterceptor('uploadImage', storage))
  AddProduct(
    @Body() inventoryCredentialsDto: InventoryCredentialsDto,
    @GetUser() user: User,
    @UploadedFile() uploadImage,
  ): Promise<any> {
    try {
      return this.inventoryService.AddProduct(
        inventoryCredentialsDto,
        user,
        uploadImage.filename,
      );
    } catch (error) {
      return error.message;
    }
  }

  //Se encarga de actualizar datos de algún producto en particular
  @Patch('/updateproduct')
  @UseInterceptors(FileInterceptor('file', { dest: './productImages' }))
  UpdateProduct(
    @Body() data,
    @UploadedFile() file,
    @GetUser() user: User,
  ): Promise<string> {
    if (file != undefined) {
      return this.inventoryService.UpdateProduct(data, user, file.filename);
    } else {
      return this.inventoryService.UpdateProduct(data, user, undefined);
    }
  }

  //Su finalidad es la de eliminar un producto registrado
  @Delete('/deleteproduct')
  DeleteProduct(@Body() data, @GetUser() user: User): Promise<string> {
    return this.inventoryService.DeleteProduct(data, user);
  }

  //Este controlador se utilia para buscar un producto en particular
  @Post('/findoneproduct')
  FindOneProduct(@Body() data, @GetUser() user: User): Promise<Product> {
    return this.inventoryService.FindOneProduct(data, user);
  }

  //Se encarga de mostrar la imagén de algún producto que se quiera visualizar
  @Post('/findproductimage')
  FindProductImage(
    @Body() data,
    @GetUser() user: User,
    @Res() res,
  ): Promise<any> {
    return this.inventoryService.FindProductImage(data, user, res);
  }

  //Se utiliza para mostrar todos los registros de un usuario en particular
  @Post('/findallproducts')
  FindAllProducts(
    @Body() data,
    @Body() options: PaginationOptionsInterface,
  ): Promise<Pagination<Product>> {
    return this.inventoryService.FindAllProducts(data, options);
  }
}
