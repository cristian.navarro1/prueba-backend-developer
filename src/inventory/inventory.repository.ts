//Importando dependencias
import { EntityRepository, Repository } from 'typeorm';
import { InventoryCredentialsDto } from './inventory dto/inventory-credentials.dto';
import { Product } from './inventory.entity';
import { User } from 'src/Auth/users.entity';

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  async createProduct(
    inventoryCredentialsDto: InventoryCredentialsDto,
    user: User,
  ): Promise<void> {
    //Obteniendo los datos proporcionados
    const { sku, name, amount, prize, description, image } =
      inventoryCredentialsDto;

    //Creando un nuevo producto con las información brindada
    const product = this.create({
      sku,
      name,
      amount,
      prize,
      description,
      image,
      user,
    });

    try {
      //Guardando el producto en la base de datos
      await this.save(product);
    } catch (error) {
      throw error;
    }
  }
}
