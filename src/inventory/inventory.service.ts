//Importando dependencias
import { Injectable, Res } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/Auth/users.entity';
import { InventoryCredentialsDto } from './inventory dto/inventory-credentials.dto';
import { Product } from './inventory.entity';
import { ProductRepository } from './inventory.repository';
import { UsersRepository } from '../Auth/users.repository';
import { join } from 'path';
import { PaginationOptionsInterface } from './paginate/paginatio.options.interface';
import { Pagination } from './paginate/pagination';
@Injectable()
export class InventoryService {
  constructor(
    @InjectRepository(ProductRepository)
    private productRepository: ProductRepository,
    private usersRepository: UsersRepository,
  ) {}

  //Método que se encarga de añadir un nuevo producto a la base de datos
  async AddProduct(
    inventoryCredentialsDto: InventoryCredentialsDto,
    user: User,
    file: string,
  ): Promise<any> {
    try {
      //Creando producto
      inventoryCredentialsDto.image = file;
      return this.productRepository.createProduct(
        inventoryCredentialsDto,
        user,
      );
    } catch (error) {
      return error.message;
    }
  }

  //Método que se utiliza para actualizar la información de
  //un producto en particular
  async UpdateProduct(data, user: User, uploadImage): Promise<string> {
    try {
      //Obteniendo valores de los campos enviados
      const { sku, name, amount, prize, description } = data;

      if (sku) {
        //Obtendiendo información de la base de datos
        const productSku = await this.productRepository.findOne({ sku, user });

        //Evaluando que campos fueron enviados por el usuario. Si existen los modificara.
        if (name) {
          productSku.name = name;
        }
        if (amount) {
          productSku.amount = amount;
        }
        if (prize) {
          productSku.prize = prize;
        }
        if (description) {
          productSku.description = description;
        }
        if (uploadImage && uploadImage != undefined) {
          productSku.image = uploadImage;
        }
        this.productRepository.save(productSku);
      } else if (name) {
        //Obtendiendo información de la base de datos
        const productName = await this.productRepository.findOne({
          name,
          user,
        });

        //Evaluando que campos fueron enviados por el usuario. Si existen los modificara.
        if (sku) {
          productName.name = name;
        }
        if (amount) {
          productName.amount = amount;
        }
        if (prize) {
          productName.prize = prize;
        }
        if (description) {
          productName.description = description;
        }
        if (uploadImage && uploadImage != undefined) {
          productName.image = uploadImage;
        }
        this.productRepository.save(productName);
      }

      return 'Datos actualizados exitosamente!';
    } catch (error) {
      throw error;
    }
  }

  //Método encargado de eliminar un producto seleccionado
  async DeleteProduct(data, user: User): Promise<string> {
    try {
      const { sku, name } = data;
      //Validando que opción coincide y elimando el producto
      if (sku) {
        this.productRepository.delete({ sku, user });
      } else if (name) {
        this.productRepository.delete({ name, user });
      }
    } catch (error) {
      throw error;
    }

    return 'Producto eliminado exitosamente';
  }

  //Método que se encarga de buscar un producto en particular
  async FindOneProduct(data, user: User): Promise<Product> {
    const { sku, name } = data;
    //Obteniendo valores de la base de datos
    const productSku = await this.productRepository.findOne({
      where: { sku, user },
    });
    const productName = await this.productRepository.findOne({
      where: { name, user },
    });

    try {
      //Validando que opción coincide y mostrando el producto
      if (productSku) {
        return productSku;
      } else if (productName) {
        return productName;
      }
    } catch (error) {
      throw error;
    }
  }

  //Método que muestra la imágen solicitada
  async FindProductImage(data, user: User, @Res() res): Promise<any> {
    try {
      //Obteniendo valores de la base de datos
      const { image } = data;
      const productImage = await this.productRepository.findOne({
        image,
        user,
      });

      //Generando la ruta donde se encuentra ubicada la imágen
      const path = join(process.cwd(), '/productImages/' + productImage.image);

      //Mostrando la imágen
      return res.sendFile(path);
    } catch (error) {
      throw error;
    }
  }

  async FindAllProducts(
    data,
    options: PaginationOptionsInterface,
  ): Promise<Pagination<Product>> {
    try {
      //Obteniendo valores de la base de datos y de los campos ingresados
      const { email } = data;
      const userProducts = await this.usersRepository.findOne({ email });
      const id = userProducts.id;

      //Buscando y contando las coincidencias existentes
      const [results, total] = await this.productRepository.findAndCount({
        where: { user: id },
        take: options.limit,
        skip: options.page - 1,
      });

      //Mostrando la paginación
      return new Pagination<Product>({
        results,
        total,
      });
    } catch (error) {
      throw error.message;
    }
  }
}
