//Aquí se define la interfaz que tendrá la paginación en cuanto a opciones
export interface PaginationOptionsInterface {
  limit: number;
  page: number;
}
