//Aquí se define la interfaz que tendrán los resultados de la paginación
export interface PaginationTResultInterface<PaginationEntity> {
  results: PaginationEntity[];
  total: number;
  next?: string;
  previous?: string;
}
