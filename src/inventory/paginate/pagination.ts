//Importando módulos a utilizar
import { PaginationTResultInterface } from './pagination.results.interface';

//Esta clase se encarga de mostrar los datos de la paginación
export class Pagination<PaginationEntity> {
  public results: PaginationEntity[];
  public total: number;

  constructor(paginatiomResults: PaginationTResultInterface<PaginationEntity>) {
    this.results = paginatiomResults.results;
    this.total = paginatiomResults.total;
  }
}
