//Importando dependencias
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './Auth/auth.module';
import { InventoryModule } from './inventory/inventory.module';

//Definiendo la configuración del módulo principal de la aplicación.
//Este se encarga de importar los módulos de la autenticación y el inventario
//así como de establecer la conexión con la base de datos
@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'postgres',
      database: 'task-management',
      autoLoadEntities: true,
      synchronize: true,
    }),
    AuthModule,
    InventoryModule,
  ],
})
export class AppModule {}
