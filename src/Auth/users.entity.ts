//Importando dependencias
import { Product } from 'src/inventory/inventory.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

//En esta entidad se define la estructura que tendrá la base de datos
@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ unique: true })
  phoneNumber: number;

  @Column({ unique: true })
  username: string;

  @Column()
  birthDate: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @OneToMany((_type) => Product, (product) => product.user, {
    cascade: true,
  })
  products: Product[];
}
