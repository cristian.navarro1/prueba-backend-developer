//Importando dependencias
import {
  Body,
  Controller,
  Patch,
  Post,
  Delete,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthCredentialsDto } from './Auth dto/auth-credentials.dto';
import { AuthService } from './auth.service';

//Aquí se define el controlador de toda la parte de la autenticación.
//Es la encargada de conectarse con los servicios de autenticación, así como
//de establecer las rutas del apartado Auth
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  //Este controlador sirve para que el cliente pueda crear un usuario dentro de la app
  //y mantener su registro en la base de datos definida
  @Post('/signup')
  SignUp(@Body() authCredentialsDto: AuthCredentialsDto): Promise<any> {
    try {
      return this.authService.SignUp(authCredentialsDto);
    } catch (error) {
      throw error;
    }
  }

  //Controlador encargado de solicitar las credenciales para iniciar sesión en
  //la base de datos. Este apartado es importante porque retorna un token de acceso
  //que servirá para validar el uso de la mayoría de los endpoints
  @Post('/signin')
  SignIn(@Body() data): Promise<{ accessToken: string }> {
    return this.authService.SignIn(data);
  }

  //Se encarga de enviarle un correo al usuario que haya olvidado su contraseña
  //junto on un link para reestablecerlo
  @Post('/forgotpassword')
  ForgotPassword(@Body() data): Promise<string> {
    return this.authService.ForgotPassword(data);
  }

  //Para acceder a este controlador se necesita del link enviado al correo
  //en el endpoint anterior. Al accesar a este podrá definirse una nueva contraseña
  @Patch('/resetpassword/:email/:token')
  ResetPassword(@Req() req, @Body() data): Promise<string> {
    return this.authService.ResetPassword(req, data);
  }

  //La función de este controlador es la de actualizar la información del usuario
  //almacenado en la base de datos en caso de ser requerido
  @UseGuards(AuthGuard())
  @Patch('/updateuser')
  UpdateUser(@Body() data): Promise<string> {
    return this.authService.UpdateUser(data);
  }

  //Este controlador tiene la finalidad de eliminar un usuario por completo de
  //la base de datos
  @UseGuards(AuthGuard())
  @Delete('/deleteuser')
  DeleteUser(@Body() data): Promise<string> {
    return this.authService.DeleteUser(data);
  }
}
