//Importando dependencias
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductRepository } from 'src/inventory/inventory.repository';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './jwt.strategy';
import { UsersRepository } from './users.repository';

//Este módulo esta encargado de definir la seguridad del usuario utilizando
//JWT, así como de importar y exportar lo necesario para que la app
//funcione sin ninhún problema
@Module({
  imports: [
    ConfigModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: 'Secret51',
      signOptions: {
        expiresIn: '15m',
      },
    }),
    TypeOrmModule.forFeature([UsersRepository, ProductRepository]),
  ],
  providers: [AuthService, JwtStrategy],
  controllers: [AuthController],
  exports: [JwtStrategy, PassportModule],
})
export class AuthModule {}
