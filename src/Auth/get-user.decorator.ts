//Importando dependencias
import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from './users.entity';

//Dedorador que servirá para validar la autorización del usuario
export const GetUser = createParamDecorator(
  (data, ctx: ExecutionContext): User => {
    const req = ctx.switchToHttp().getRequest();
    return req.user;
  },
);
