//Importando dependencias
import { EntityRepository, Repository } from 'typeorm';
import { AuthCredentialsDto } from './Auth dto/auth-credentials.dto';
import { User } from './users.entity';
import * as bcrypt from 'bcrypt';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {
  async createUser(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    //Obteniendo los datos proporcionados
    const { name, phoneNumber, username, birthDate, email, password } =
      authCredentialsDto;

    //Aplicación de HashedPasswords para agregar mayor seguridad a la aplicación
    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    //Creando un nuevo usuario con los datos obtenidos
    const user = this.create({
      name,
      phoneNumber,
      username,
      birthDate,
      email,
      password: hashedPassword,
    });

    try {
      //Guardando el usuario creado en la base de datos
      await this.save(user);
    } catch (error) {
      throw error;
    }
  }
}
