//Importando dependencias
import { Injectable, UnauthorizedException, Req } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthCredentialsDto } from './Auth dto/auth-credentials.dto';
import { UsersRepository } from './users.repository';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from './jwt-payload.interface';
import * as nodemailer from 'nodemailer';
import jwt = require('jsonwebtoken');
import { ProductRepository } from 'src/inventory/inventory.repository';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersRepository)
    private usersRepository: UsersRepository,
    private productRepository: ProductRepository,
    private jwtService: JwtService,
  ) {}

  //Método para crear un nuevo usuario
  async SignUp(authCredentialsDto: AuthCredentialsDto): Promise<any> {
    try {
      return this.usersRepository.createUser(authCredentialsDto);
    } catch (error) {
      console.error(error);
      throw error.message;
    }
  }

  //Método para iniciar sesión y generar un token de acqceso
  async SignIn(data): Promise<{ accessToken: string }> {
    //Obteniendo valores de la base de datos y de los campos enviados
    const { email, password } = data;
    const user = await this.usersRepository.findOne({ email });

    try {
      //Comprobando que la información proporcionada por el usuario.
      //Si es así retorna el token de acceso, sino genera una advertencia.
      if (user && (await bcrypt.compare(password, user.password))) {
        const payload: JwtPayload = { email };
        const accessToken: string = await this.jwtService.sign(payload, {
          expiresIn: '15m',
        });
        return { accessToken };
      } else {
        throw new UnauthorizedException(
          'Email o contraseña incorrectos. Por favor revise sus credenciales',
        );
      }
    } catch (error) {
      console.error(error);
      throw error.message;
    }
  }

  //Método que se encarga de enviarle un correo al usuario junto con un link de recuperación
  async ForgotPassword(data): Promise<string> {
    try {
      //Obteniendo valores de la base de datos y de los campos enviados
      const { email } = data;
      const user = await this.usersRepository.findOne({ email });

      //En esta sección se genera un link el cual contiene un token que solo
      //permitira que este link sea válido durante 1t minutos
      const payload: JwtPayload = { email };
      const secret = 'Secret51' + user.password;
      const token: string = await jwt.sign(payload, secret, {
        expiresIn: '15m',
      });
      const link = `https://localhost:3000/auth/resetpassword/${user.email}/${token}`;

      //Los siguientes fragmentos de código se encargan de enviar el correo
      //junto con el link al usuario que haya olvidado su contraseña
      const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'pruebabackend.elaniin@gmail.com',
          pass: 'pruebaElaniin21',
        },
      });

      const mailOptions = {
        from: 'Prueba Backend Developer',
        to: `${email}`,
        subject: 'Recuperación de contraseña',
        html: `
        <h3>Hola ${user.name}!</h3>
        <p>Por favor use este <a href="${link}">link</a> para reestablecer su contraseña.</p>
        `,
      };

      transporter.sendMail(mailOptions, (err, info) => {
        if (err) console.log(err);
        else console.log('Email enviado: ' + info.response);
      });

      return 'Revise la información enviada a su correo';
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  //Este método sirve para definir una nueva contraseña en caso de olvido
  async ResetPassword(@Req() req, data): Promise<string> {
    try {
      //Obteniendo valores de la base de datos y de los campos enviados
      const { email, token } = req.params;
      const { password } = data;
      const user = await this.usersRepository.findOne({ email });
      const secret = 'Secret51' + user.password;
      const payload = jwt.verify(token, secret);

      //En esta sección se valida si el link ingresado es válido. Si es así,
      //crea una nueva hashedPassword y la almcacena en la base de datos
      if (user && payload) {
        const salt = await bcrypt.genSalt();
        const hashedPassword = await bcrypt.hash(password, salt);

        user.password = hashedPassword;
        this.usersRepository.save(user);

        return 'Contraseña modificada con éxito!';
      }
    } catch (error) {
      throw error.message;
    }
  }

  //Método para actualizar los datos del usuario
  async UpdateUser(data): Promise<string> {
    try {
      //Obteniendo valores de los campos enviados
      const { name, phoneNumber, username, birthDate, email, password } = data;
      if (email) {
        //Obteniendo valores de la base de datos
        const userEmail = await this.usersRepository.findOne({ email });

        //Verificando si el email del usuario es válido
        if (!userEmail) {
          throw new UnauthorizedException(
            'Email no encontrado. Por favor revise sus credenciales',
          );
        }

        //Evaluando que campos fueron enviados por el usuario. Si existe los modificara.
        if (name) {
          userEmail.name = name;
        }
        if (phoneNumber) {
          userEmail.phoneNumber = phoneNumber;
        }
        if (username) {
          userEmail.username = username;
        }
        if (birthDate) {
          userEmail.birthDate = birthDate;
        }
        if (password) {
          userEmail.password = password;
        }
        this.usersRepository.save(userEmail);
      } else if (username) {
        //Obteniendo valores de la base de datos
        const userUsername = await this.usersRepository.findOne({ username });

        //Verificandoo si el nombre de usuario proporcionado es válido
        if (!userUsername) {
          throw new UnauthorizedException(
            'Nombre de usuario no encontrado. Por favor revise sus credenciales',
          );
        }

        //Evaluando que campos fueron enviados por el usuario. Si existe los modificara.
        if (name) {
          userUsername.name = name;
        }
        if (phoneNumber) {
          userUsername.phoneNumber = phoneNumber;
        }
        if (email) {
          userUsername.email = email;
        }
        if (birthDate) {
          userUsername.birthDate = birthDate;
        }
        if (password) {
          userUsername.password = password;
        }
        this.usersRepository.save(userUsername);
      }

      return 'Datos actualizados exitosamente!';
    } catch (error) {
      throw error;
    }
  }

  //Método para eliminar un usuario por completo de la base de datos
  async DeleteUser(data): Promise<string> {
    //Obteniendo valores de la base de datos y de los campos enviados
    const { email } = data;
    const user = await this.usersRepository.findOne({ email });

    //Verificando si el email ingresado es válido
    if (!user) {
      throw new UnauthorizedException(
        'Email no encontrado. Por favor revise sus credenciales',
      );
    }

    try {
      //Eliminando al usuario de los registros
      this.productRepository.delete(user.id);
      this.usersRepository.delete(user);
    } catch (error) {
      throw error;
    }

    return 'Usuario eliminado exitosamente';
  }
}
