# Prueba Backend Developer

## Descripción

Este proyecto consiste en una API que permite la creación de usuarios los cuales pueden tener un inventario, es decir, cada usuario puede almacenar en la base de datos el registro detallado de cada uno de los productos que dispone, llevando así un control de lo que posee.

Para el uso correcto de esta API a continuación explicaré cada uno de los endpoints disponibles:

- [Colección de postman](#coleccion-de-postman)
- [Sign Up](#sign-up)
- [Sign In](#sign-in)
- [Forgot password](#forgot-password)
- [Reset password](#reset-password)
- [Update user](#update-user)
- [Delete user](#delete-user)
- [Add product](#add-product)
- [Update product](#update-product)
- [Delete product](#delete-product)
- [Find one product](#find-one-product)
- [Find product image](#find-product-image)
- [Find all products](#find-all-products)

##
### Colección de postman
##

Para poder hacer uso de esta API, comparto el siguiente `link` que servirá para importar esta colección, la cual contiene todos los endpoints con los campos a llenar en cada uno.

Cabe destacar que cada endpoint tiene su particularidad, algunos requieren un token de acceso y otros no, o en el caso de otros requiere una manera diferente de enviar datos, pero en general todos los endpoints deberán enviar los campos con su valor en la sección de `Body`, mediante el tipo de dato `x-www-form-urlencode`. En el caso de el token de acceso, en los endpoints necesarios se debera utilizar en la sección de `Authorization`, seleccionando el tipo de token llamado `Bearer token` y posteriormente pegandolo en el campo destinado.

El link de la colección es el siguiente:

`https://www.getpostman.com/collections/0df1f539e96cfae42db4`

##
### Sign Up
##

Este endpoint sirve para crear un nuevo usuario. Para ello se deben de llenar los campos preexistentes en la `Colección de postman`.

`localhost:3000/auth/signup`

`No requiere token de acceso`

##
### Sign In
##

Se utiliza para iniciar sesión dentro de la API. Además genera el token de acceso que será necesario para acceder a más funcionalidades.

`localhost:3000/auth/signin`

`No requiere token de acceso`

##
### Forgot password
##

Su finalidad es la de en caso de que un usuario olvidé su contraseña, este endpoint se encargará de enviarle un correo junto con un `link` que contiene lo necesario para crear una nueva contraseña.

`localhost:3000/auth/forgotpassword`

`No requiere token de acceso`

##
### Reset password
##

Este endpoint viene de la mano del anterior. Básicamente es el encargado de permitir que el usuario que olvidó su contraseña pueda crear una nueva, para lo cual necesita el link enviado en la sección de `Forgot password`, sin este no es posible utilizarlo.

`No requiere token de acceso`

##
### Update user
##

En caso de que el usuario desee realizar una actualización de sus datos de usuario, este endpoint es el indicado. Para ello es necesario enviarle o bien el `email` o el `username`, ya que servirá para buscar a cual usuario hace referencia.

`localhost:3000/auth/updateuser`

`Requiere token de acceso`

##
### Delete user
##

Si se desea eliminar a un usuario de la base de datos se debe utilizar este apartado. Cabe recalcar que si se borra un usuario también se eliminaran todos los productos de su inventario, no quedará registro, por lo que es necesario tener mucha precaución en su uso.

`localhost:3000/auth/deleteuser`

`Requiere token de acceso`

##
### Add product
##

Para agregar un producto al inventario se debera enviar la información en los campos de la `Colección de postman`.

`localhost:3000/inventory/addproduct`

`Requiere token de acceso`

##
### Update product
##

Si se desea modificar la información de un producto se debera proporcionar el `sku` o en su defecto el `name`, esto para poder encontrar al producto que requiera un cambio.

`localhost:3000/inventory/updateproduct`

`Requiere token de acceso`

##
### Delete product
##

En caso de que se quiera eliminar un producto del inventario, este es el endpoint adecuado.

`localhost:3000/inventory/deleteproduct`

`Requiere token de acceso`

##
### Find one product
##

Si se desea ver la información de un producto en particular, se debe proporcionar el `sku` o el `name` del producto.

`localhost:3000/inventory/findoneproduct`

`Requiere token de acceso`

##
### Find product image
##

En caso de que se necesite visualizar la imagen de un producto en especifico, se puede hacer desde este endpoint. Únicamente se debe de proporcionar el nombre de la imagen brindado en el endpoint de `Find one product` o `Find all products`.

`localhost:3000/inventory/findproductimage`

`Requiere token de acceso`

##
### Find all products
##

Si se desean ver todos los productos que un usuario posee, se puede hacer mediante este endpoint.

`localhost:3000/inventory/findallproducts`

`Requiere token de acceso`
